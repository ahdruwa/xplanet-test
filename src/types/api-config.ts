export interface IOpenWeatherApiConfig {
  token: string;
  baseUrl: string;
}
