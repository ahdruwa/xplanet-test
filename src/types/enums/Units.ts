export enum Units {
  Kelvin = 'Kelvin',
  Celsius = 'Celsius',
  Fahrenheit = 'Fahrenheit',
}
