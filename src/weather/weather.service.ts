import { Injectable } from '@nestjs/common';
import { concatMap, map } from 'rxjs';
import { GeocodingService } from 'src/geocoding/geocoding.service';
import { OpenWeatherApiService } from 'src/open-weather-api/open-weather-api.service';
import { Units } from 'src/types/enums/Units';

@Injectable()
export class WeatherService {
  constructor(
    private readonly geocodingService: GeocodingService,
    private readonly apiService: OpenWeatherApiService,
  ) {}

  getWeatherByCityName({
    cityName,
    units,
  }: {
    cityName: string;
    units?: string;
  }) {
    const coordinatesStream = this.geocodingService.getCoordinates({
      cityName,
    });

    return coordinatesStream.pipe(
      concatMap((coords) =>
        this.apiService.callApi('weather', {
          ...coords,
          units,
        }),
      ),
    );
  }

  unitsMap = {
    [Units.Celsius]: 'metric',
    [Units.Fahrenheit]: 'imperial',
    [Units.Kelvin]: 'standart',
  };

  getTemperature({ cityName, units }: { cityName: string; units: Units }) {
    const mappedUnit = this.unitsMap[units] || 'metric';
    const weatherStream = this.getWeatherByCityName({
      cityName,
      units: mappedUnit,
    });

    return weatherStream.pipe(
      map((weather) => ({
        temperature: weather?.main?.temp,
        units,
      })),
    );
  }

  getWindSpeed({ cityName }: { cityName: string }) {
    const weatherStream = this.getWeatherByCityName({ cityName });

    return weatherStream.pipe(
      map((weather) => ({
        windSpeed: weather?.wind?.speed,
        units: weather?.wind?.speed ? 'meter/sec' : undefined,
      })),
    );
  }
}
