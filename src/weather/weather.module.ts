import { Module } from '@nestjs/common';
import { WeatherService } from './weather.service';
import { WeatherController } from './weather.controller';
import { GeocodingModule } from 'src/geocoding/geocoding.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import weatherConfig from 'src/config/weather.config';
import { OpenWeatherApiModule } from 'src/open-weather-api/open-weather-api.module';

@Module({
  imports: [
    GeocodingModule,
    ConfigModule.forFeature(weatherConfig),
    OpenWeatherApiModule.forFeature({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        token: configService.get('weather.token'),
        baseUrl: configService.get('weather.baseUrl'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [WeatherService],
  controllers: [WeatherController],
})
export class WeatherModule {}
