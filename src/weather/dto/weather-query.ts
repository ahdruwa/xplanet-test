import { ApiProperty } from '@nestjs/swagger';
import { IsEnum, IsOptional } from 'class-validator';
import { Units } from 'src/types/enums/Units';
import { WeatherBaseDTO } from './weather-base';

export class GetWeatherQueryDTO extends WeatherBaseDTO {
  @ApiProperty({
    enum: Units,
    required: false,
  })
  @IsOptional()
  @IsEnum(Units)
  units: Units = Units.Celsius;
}
