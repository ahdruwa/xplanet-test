import { ApiProperty } from '@nestjs/swagger';
import { Units } from 'src/types/enums/Units';

export class TemperatureResponse {
  @ApiProperty({
    example: 32,
    description: 'Degrees',
  })
  temperature: number;

  @ApiProperty({
    example: Units.Celsius,
    description: 'Provided units',
  })
  units: number;
}
