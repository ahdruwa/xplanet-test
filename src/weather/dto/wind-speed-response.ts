import { ApiProperty } from '@nestjs/swagger';

export class WindSpeed {
  @ApiProperty({
    example: 4.17,
  })
  windSpeed: number;

  @ApiProperty({
    example: 'meter/sec',
  })
  units: string;
}
