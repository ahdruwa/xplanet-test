import { Controller, Get, Query, ValidationPipe } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';
import { GeocodingService } from 'src/geocoding/geocoding.service';
import { TemperatureResponse } from './dto/temperature-response';
import { WeatherBaseDTO } from './dto/weather-base';
import { GetWeatherQueryDTO } from './dto/weather-query';
import { WindSpeed } from './dto/wind-speed-response';
import { WeatherService } from './weather.service';

@ApiTags('weather')
@Controller('api/weather')
export class WeatherController {
  constructor(
    private readonly geocodingService: GeocodingService,
    private readonly weatherService: WeatherService,
  ) {}

  @Get('temperature')
  @ApiResponse({
    status: 404,
    description: 'If `cityName` is not correct',
  })
  @ApiResponse({
    status: 200,
    type: TemperatureResponse,
  })
  getTemperature(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
        forbidNonWhitelisted: true,
      }),
    )
    query: GetWeatherQueryDTO,
  ) {
    const { cityName, units } = query;
    return this.weatherService.getTemperature({ cityName, units });
  }

  @Get('wind')
  @ApiResponse({
    status: 404,
    description: 'If `cityName` is not correct',
  })
  @ApiResponse({
    status: 200,
    type: WindSpeed,
  })
  getWind(
    @Query(
      new ValidationPipe({
        transform: true,
        transformOptions: { enableImplicitConversion: true },
        forbidNonWhitelisted: true,
      }),
    )
    query: WeatherBaseDTO,
  ) {
    const { cityName } = query;
    return this.weatherService.getWindSpeed({ cityName });
  }
}
