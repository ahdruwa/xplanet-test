import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import geocodingConfig from 'src/config/geocoding.config';
import { OpenWeatherApiModule } from 'src/open-weather-api/open-weather-api.module';
import { GeocodingService } from './geocoding.service';

@Module({
  imports: [
    HttpModule,
    ConfigModule.forFeature(geocodingConfig),
    OpenWeatherApiModule.forFeature({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        token: configService.get('geocoding.token'),
        baseUrl: configService.get('geocoding.baseUrl'),
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [GeocodingService],
  exports: [GeocodingService],
})
export class GeocodingModule {}
