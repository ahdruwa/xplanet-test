export type GeocodingBaseQuery = {
  appid: string;
};

export type GetCoordinatesQueryParams = {
  q: string;
  limit?: number;
};
