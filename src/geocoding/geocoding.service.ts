import { Injectable, NotFoundException } from '@nestjs/common';
import { concatMap, first, map } from 'rxjs';
import { OpenWeatherApiService } from 'src/open-weather-api/open-weather-api.service';
import { GetCoordinatesQueryParams } from './types/api';

@Injectable()
export class GeocodingService {
  constructor(private readonly apiService: OpenWeatherApiService) {}

  getCoordinates({ cityName }: { cityName: string }) {
    const directData = this.apiService.callApi<GetCoordinatesQueryParams>(
      'direct',
      {
        q: cityName,
      },
    );

    const coordinates = directData.pipe(
      concatMap((data) => {
        if (data.length) {
          return data;
        }

        throw new NotFoundException();
      }),
      first(),
      map((data) => data),
      map(({ lat, lon }) => ({ lat, lon })),
    );

    return coordinates;
  }
}
