import { registerAs } from '@nestjs/config';

export default registerAs('geocoding', () => ({
  token: process.env.WEATHER_API_KEY,
  baseUrl: process.env.GEOCODING_API_BASE_URL,
}));
