import { registerAs } from '@nestjs/config';

export default registerAs('weather', () => ({
  token: process.env.WEATHER_API_KEY,
  baseUrl: process.env.WEATHER_API_BASE_URL,
}));
