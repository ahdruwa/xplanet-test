import { registerAs } from '@nestjs/config';
import { utilities } from 'nest-winston';
import { format, transports } from 'winston';

export default registerAs('logger', () => ({
  transports: [
    new transports.Console({
      format: format.combine(
        format.timestamp(),
        format.ms(),
        utilities.format.nestLike(process.env.APP_NAME, {
          prettyPrint: true,
        }),
        format.errors({
          stack: true,
        }),
      ),
    }),
    new transports.File({
      format: format.json(),
      filename: 'logs/error.log',
      level: 'error',
    }),
    new transports.File({
      filename: 'logs/combined.log',
      format: format.json(),
    }),
  ],
}));
