import geocodingConfig from './geocoding.config';
import loggerConfig from './logger.config';
import weatherConfig from './weather.config';

export default () => ({
  logger: loggerConfig(),
  weather: weatherConfig(),
  geocoding: geocodingConfig(),
});
