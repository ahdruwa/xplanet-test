import { Module } from '@nestjs/common';
import { WeatherModule } from './weather/weather.module';
import { GeocodingModule } from './geocoding/geocoding.module';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { OpenWeatherApiModule } from './open-weather-api/open-weather-api.module';
import { WinstonModule } from 'nest-winston';
import config from './config';

@Module({
  imports: [
    WeatherModule,
    GeocodingModule,
    ConfigModule.forRoot({
      envFilePath: '.env',
      load: [config],
    }),
    OpenWeatherApiModule,
    WinstonModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => ({
        transports: configService.get('logger.transports'),
      }),
      inject: [ConfigService],
    }),
  ],
})
export class AppModule {}
