import { HttpModule } from '@nestjs/axios';
import {
  DynamicModule,
  FactoryProvider,
  Module,
  ModuleMetadata,
} from '@nestjs/common';
import { OPEN_WEATHER_API_CONFIG } from 'src/constants/tokens';
import { OpenWeatherApiService } from './open-weather-api.service';

type OpenWeatherAPIProvider = Omit<FactoryProvider, 'provide'> & {
  imports: ModuleMetadata['imports'];
};

@Module({})
export class OpenWeatherApiModule {
  // @todo: Фактически -- костыль, хорошо бы заиспользовать `@nestjsplus/dyn-schematics`
  // и вынести в отдельную библиотеку
  static forFeature(provider: OpenWeatherAPIProvider): DynamicModule {
    return {
      imports: [HttpModule, ...provider.imports],
      providers: [
        {
          ...provider,
          provide: OPEN_WEATHER_API_CONFIG,
        },
        OpenWeatherApiService,
      ],
      exports: [OpenWeatherApiService],
      module: OpenWeatherApiModule,
    };
  }
}
