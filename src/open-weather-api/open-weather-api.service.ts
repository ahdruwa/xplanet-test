import { Inject, Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { map } from 'rxjs';
import { IOpenWeatherApiConfig } from 'src/types/api-config';
import { OPEN_WEATHER_API_CONFIG } from 'src/constants/tokens';

@Injectable()
export class OpenWeatherApiService {
  baseUrl: string;
  token: string;

  constructor(
    @Inject(OPEN_WEATHER_API_CONFIG)
    config: IOpenWeatherApiConfig,
    private readonly httpService: HttpService,
  ) {
    const { baseUrl, token } = config;
    this.baseUrl = baseUrl;
    this.token = token;
  }

  callApi<T>(method: string, query?: T) {
    const url = new URL(`${this.baseUrl}/${method}`);
    const urlParams = url.searchParams;
    urlParams.append('appid', this.token);

    for (const [queryKey, queryValue] of Object.entries(query)) {
      urlParams.append(queryKey, queryValue);
    }

    const responseStream = this.httpService.get(url.toString());

    return responseStream.pipe(map((response) => response?.data));
  }
}
